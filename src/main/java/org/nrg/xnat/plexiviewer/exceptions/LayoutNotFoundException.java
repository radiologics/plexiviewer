/*
 * org.nrg.xnat.plexiviewer.exceptions.LayoutNotFoundException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.exceptions;

public class LayoutNotFoundException extends Exception {
    public LayoutNotFoundException() {
    }

    public LayoutNotFoundException(String msg) {
        super(msg);
    }
}
