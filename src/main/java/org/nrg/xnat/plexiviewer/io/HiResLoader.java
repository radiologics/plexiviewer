/*
 * org.nrg.xnat.plexiviewer.io.HiResLoader
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xnat.plexiviewer.io;

import ij.ImagePlus;
import org.nrg.xnat.plexiviewer.converter.ConverterUtils;
import org.nrg.xnat.plexiviewer.exceptions.InvalidParameterValueException;
import org.nrg.xnat.plexiviewer.lite.UserSelection;
import org.nrg.xnat.plexiviewer.lite.display.MontageDisplay;
import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;
import org.nrg.xnat.plexiviewer.lite.xml.Layout;
import org.nrg.xnat.plexiviewer.lite.xml.MontageView;
import org.nrg.xnat.plexiviewer.lite.xml.ViewableItem;
import org.nrg.xnat.plexiviewer.manager.PlexiSpecDocReader;
import org.nrg.xnat.plexiviewer.utils.FileUtils;
import org.nrg.xnat.plexiviewer.utils.transform.PlexiImageOrientor;
import org.nrg.xnat.plexiviewer.utils.transform.ReOrientMakeMontage;

import java.io.File;

public class HiResLoader implements ImageLoaderI {
    private UserSelection userOptions;
    MontageView    mView;
    Layout         layout;
    MontageDisplay mDisplay;
    PlexiImageFile openedImageFile;

    public HiResLoader(UserSelection opt) {
        userOptions = opt;
    }

    public ImagePlus load() throws InvalidParameterValueException {
        ImagePlus    image = null;
        ViewableItem vi    = PlexiSpecDocReader.GetInstance().getSpecDoc(userOptions.getProject()).getViewableItem(userOptions.getDataType());
        mView = vi.getHiRes().getMontageView();
        layout = vi.getHiRes().getLayout();
        String format = vi.getHiRes().getFormat();
        mDisplay = new MontageDisplay(mView, userOptions.getOrientation().toUpperCase(), layout);
        PlexiImageFile hpf = userOptions.getFile();
        if (hpf == null) {
            hpf = FileUtils.getHiResFilePath(userOptions.getSessionId(), userOptions.getProject(), userOptions.getDataType(), userOptions.getHiResLayerNum(), userOptions.getScanNo());
        }
        System.out.println("HIResLoader file details are " + hpf.toString());
        PlexiImageFile pf = new PlexiImageFile();
        if (format.equalsIgnoreCase("DICOM")) {
            pf = hpf;
            System.out.println("The MontageView is " + mView);
            PlexiFileOpener pfo = new PlexiFileOpener(format, pf);
            image = pfo.getImagePlus();
            PlexiImageOrientor pio = new PlexiImageOrientor(image, "DICOM");
            image = pio.getImage(pfo.getOrientation(), userOptions.getOrientation());
            ReOrientMakeMontage rm = new ReOrientMakeMontage(userOptions, format, getMontageDisplay());
            image = rm.doPerform(image);
        } else {
            pf.setPath(hpf.getCachePath());
            pf.setName(FileUtils.getLoResFileName(hpf.getName(), "HiRes", userOptions.getOrientation()));
            pf = FileUtils.fileExists(pf);
            if (pf == null) {
                System.out.println("File doesnt exists ");
                //PlexiPublisher publisher = PlexiStatusPublisherManager.GetInstance().getPublisher(userOptions.toString());
                //if (publisher!=null)
                //	publisher.setValue("Building required Image....Please wait");
                userOptions.setFile(hpf);
                pf = ConverterUtils.convert(userOptions);
                if (pf == null) {
                    System.out.println("Couldnt launch the conveter");
                    return null;
                }
            }
            System.out.println("Looking out for the Hi Res Oriented file " + pf.getPath() + File.separator + pf.getName());
            if (format.equalsIgnoreCase("IMA")) {
                format = "IFH";
            }
            PlexiFileOpener pfo = new PlexiFileOpener(format, pf);
            image = pfo.getImagePlus();
            ReOrientMakeMontage rm = new ReOrientMakeMontage(userOptions, format, getMontageDisplay());
            image = rm.doPerform(image);
        }
        openedImageFile = pf;
        return image;
    }

    public PlexiImageFile getPlexiImageFile() {
        return openedImageFile;
    }

    public MontageDisplay getMontageDisplay() {
        return mDisplay;
    }
}
