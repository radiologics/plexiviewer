/*
 * org.nrg.xnat.plexiviewer.io.ImageLoaderI
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xnat.plexiviewer.io;

import ij.ImagePlus;
import org.nrg.xnat.plexiviewer.lite.display.MontageDisplay;
import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;


public interface ImageLoaderI {
    public ImagePlus load() throws Exception;

    public MontageDisplay getMontageDisplay();

    public PlexiImageFile getPlexiImageFile();

}
