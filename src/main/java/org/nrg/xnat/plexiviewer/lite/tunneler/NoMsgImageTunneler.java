/*
 * org.nrg.xnat.plexiviewer.lite.tunneler.NoMsgImageTunneler
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.tunneler;

import ij.ImageStack;
import org.nrg.xnat.plexiviewer.lite.UserSelection;
import org.nrg.xnat.plexiviewer.lite.display.MontageDisplay;
import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;
import org.nrg.xnat.plexiviewer.lite.manager.PlexiImageViewerI;
import org.nrg.xnat.plexiviewer.lite.ui.LoadStatus;
import org.nrg.xnat.plexiviewer.lite.utils.HTTPDetails;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class NoMsgImageTunneler implements Runnable {
    private URL           dataURL;
    private String        host;
    private URLConnection servletConnection;
    private boolean       windowShowing;
    Thread            queryRetriever;
    PlexiImageViewerI image;
    UserSelection     userSelection;
    int               index;

    public NoMsgImageTunneler(PlexiImageViewerI i) {
        image = i;
        this.host = HTTPDetails.getHost();
        windowShowing = false;
        userSelection = null;
    }

    private void openConnection() {
        String suffix = HTTPDetails.getSuffix("ImageDistributorServlet");
        try {
            dataURL = new URL(HTTPDetails.getProtocol(), HTTPDetails.getHost(), HTTPDetails.getPort(), suffix);
            servletConnection = HTTPDetails.openConnection(dataURL);
            servletConnection.setDoInput(true);
            servletConnection.setDoOutput(true);
            //Don't use a cached version of URL connection.
            servletConnection.setUseCaches(false);
            servletConnection.setDefaultUseCaches(false);
            //Specify the content type that we will send binary data
            servletConnection.setRequestProperty("Content-Type", "application/octet-stream");
            ObjectOutputStream outStreamToServlet = new ObjectOutputStream(servletConnection.getOutputStream());
            outStreamToServlet.writeObject(userSelection);
            outStreamToServlet.flush();
            outStreamToServlet.close();
        } catch (MalformedURLException mfe) {
            mfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


    public void load(UserSelection u, int i) {
        index = i;
        userSelection = u;
        queryRetriever = new Thread(this, "NoMsgImageTunneler");
        queryRetriever.start();
    }


    public void run() {
        try {
            retrieveImage();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (OutOfMemoryError ome) {
            image.handleOutOfMemoryError();
        }
    }

    private void retrieveImage() throws IOException {
        openConnection();
        InputStream       is = servletConnection.getInputStream();
        ObjectInputStream in = new ObjectInputStream(is);
        try {
            LoadStatus status = (LoadStatus) in.readObject();
            if (!status.isSuccess()) {
                PlexiImageFile pf = new PlexiImageFile();
                pf.setDimX(200);
                pf.setDimY(200);
                pf.setDimZ(0);
                //LoadErrorDialog led = new LoadErrorDialog(image.getImagePlus().getWindow(), status.getMessage(), false);
                //led.show();
                //image.getImagePlus().getWindow().close();
                return;
            }

            int            nSlices = status.getCount();
            int            width   = status.getDimensions().width;
            int            height  = status.getDimensions().height;
            Object         pixels;
            ImageStack     stack   = new ImageStack(width, height);
            MontageDisplay display = (MontageDisplay) in.readObject();

            for (int i = 0; i < nSlices; i++) {
                pixels = (Object) in.readObject();
                stack.addSlice("", pixels);
            }
            image.setImage(index, stack);
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (OutOfMemoryError ome) {
            image.handleOutOfMemoryError();
        }
        in.close();
        is.close();
    }

}
