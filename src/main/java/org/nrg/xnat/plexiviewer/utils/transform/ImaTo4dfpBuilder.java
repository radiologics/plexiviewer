/*
 * org.nrg.xnat.plexiviewer.utils.transform.ImaTo4dfpBuilder
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plexiviewer.utils.transform;

import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;
import org.nrg.xnat.plexiviewer.utils.FileUtils;
import org.nrg.xnat.plexiviewer.utils.PlexiConstants;

import java.io.IOException;

public class ImaTo4dfpBuilder {

    PlexiImageFile pf;
    //  String filename;

    public ImaTo4dfpBuilder(PlexiImageFile pf) { //, String fname) {
        this.pf = pf;
 /*       this.filename = fname;
        if (filename.endsWith(".img")) {
        	int i = filename.lastIndexOf(".img");
        	if (i !=-1) {
        		filename = filename.substring(0,i);
        	}
        }
 */
    }

    public boolean create() {
        boolean success = true;
        if (pf.getFormat() != null && pf.getFormat().equalsIgnoreCase("IMA")) {
            if (!exists4dfpVersionForIma()) {
                success = buildImaImage();
            }
            pf.setPath(pf.getCachePath());
            //pf.setName(filename+".4dfp.img");
            pf.setName(pf.getName() + ".4dfp.img");
            pf.setXsiType(PlexiConstants.PLEXI_IMAGERESOURCE);
            pf.setFormat("IFH");
            try {
                if (pf.getPath().endsWith("/")) {
                    pf.setURIAsString(pf.getPath() + pf.getName());
                } else {
                    pf.setURIAsString(pf.getPath() + "/" + pf.getName());
                }
            } catch (Exception e) {
                System.out.println("URI Exception " + pf.getPath() + " " + pf.getName());
            }
        }
        return success;
    }

    private boolean exists4dfpVersionForIma() {
        return FileUtils.fileExists(pf.getCachePath(), pf.getName() + ".4dfp.img");
    }

    private boolean buildImaImage() {
        String cachePath = pf.getCachePath();
        if (cachePath.startsWith("file:")) {
            cachePath = cachePath.substring(5);
        }
//        String launchProcess = "arc-build-RawMprage -d " + pf.getPath() + " -f " + pf.getName() + " -o " + cachePath +  "  -w " + filename;
        String launchProcess = "arc-build-RawMprage -d " + pf.getPath() + " -f " + pf.getName() + " -o " + cachePath;

        System.out.println("Attempting to launch script..." + launchProcess);
        int rtnValue = 1;
        try {
            Process p = Runtime.getRuntime().exec(launchProcess);
            rtnValue = p.waitFor();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        System.out.println("Return value from process is " + rtnValue);
        if (rtnValue != 0) {
            System.out.println("Imato4dfpBuilder::Unable to build Ima launch script..." + launchProcess);
        }
        return ((rtnValue == 0) ? true : false);
    }
}
