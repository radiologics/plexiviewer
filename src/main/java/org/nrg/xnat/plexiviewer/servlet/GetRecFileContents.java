/*
 * org.nrg.xnat.plexiviewer.servlet.GetRecFileContents
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.servlet;

import org.nrg.xnat.plexiviewer.io.IOHelper;
import org.nrg.xnat.plexiviewer.lite.UserSelection;
import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;
import org.nrg.xnat.plexiviewer.lite.utils.LiteFileUtils;
import org.nrg.xnat.plexiviewer.reader.ReadFileContents;
import org.nrg.xnat.plexiviewer.utils.ArchivePathManager;
import org.nrg.xnat.plexiviewer.utils.PlexiConstants;
import org.nrg.xnat.plexiviewer.utils.URIUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GetRecFileContents extends HttpServlet {

    /**
     * Initializes the servlet
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

    }

    /**
     * Destroys the servlet
     */
    public void destroy() {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {
        ObjectOutputStream out = new ObjectOutputStream(response.getOutputStream());
        try {
            System.out.println("GetRecFileContents servlet invoked ");
            String            contentType       = "application/x-java-serialized-object";
            ObjectInputStream inputFromClient   = new ObjectInputStream(request.getInputStream());
            UserSelection     userOptions       = (UserSelection) inputFromClient.readObject();
            String            rtn;
            PlexiImageFile    pf                = null;
            String            cachePathLocation = ArchivePathManager.GetInstance().getCachePathLocation(userOptions.getProject(), userOptions.getSessionLabel());

            if (userOptions.hasFile()) {
                pf = userOptions.getFile();
            } else if (userOptions.hasXnatFile()) {
                pf = IOHelper.getPlexiImageFile(userOptions.getXnatFile(), cachePathLocation, userOptions.getFormattedWindowTitle() + "_" + LiteFileUtils.getFileName(userOptions.getXnatFile()));
            }
            if (pf.getXsiType().equals(PlexiConstants.PLEXI_IMAGERESOURCE) || pf.getXsiType().equals(PlexiConstants.XNAT_IMAGERESOURCE)) {
                URIUtils u           = new URIUtils(pf.getURIAsString());
                String   path        = u.getPath();
                String   name        = u.getName();
                String   recFileName = path + File.separator + name + ".rec";
                File     recFile     = new File(recFileName);
                out.writeObject(ReadFileContents.getContents(recFile));
                out.flush();
            }
        } catch (Exception e) {
            System.out.println("Handling exception... ");
            out.writeObject("Rec File Details not available\n");
            out.flush();
        }
    }


    /**
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }

}
