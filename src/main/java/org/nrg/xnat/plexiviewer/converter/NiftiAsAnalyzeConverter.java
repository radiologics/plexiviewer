/*
 * org.nrg.xnat.plexiviewer.converter.NiftiAsAnalyzeConverter
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plexiviewer.converter;

import ij.ImagePlus;
import org.nrg.xnat.plexiviewer.io.PlexiFileOpener;
import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;

public class NiftiAsAnalyzeConverter extends NonXnatConverter {

    public NiftiAsAnalyzeConverter(String fPath, String fFileName, String tPath, String inOrientation) {
        super(fPath, fFileName, tPath);
        baseOrientation = inOrientation;
    }

    protected ImagePlus openBaseFile(PlexiImageFile pf) {
        if (format == null) {
            System.out.println("Missing format");
            return null;
        }
        PlexiFileOpener pfo   = new PlexiFileOpener(format, pf);
        ImagePlus       image = null;
        image = pfo.getImagePlus();

        if (image == null) {
            System.out.println("Image Converter....couldnt find the  Image File");
            return null;
        }
        return image;
    }


}
